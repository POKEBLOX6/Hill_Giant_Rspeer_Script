import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;

public class BankItems extends Task {

    @Override
    public boolean validate() {//Do I execute, true or false
        return InBank();
    }

    @Override
    public int execute() {
        Main.TestText = "Banking";
        if (Main.Edgeville_bank.contains(Players.getLocal())) {
            if (Bank.isClosed()) {
                Bank.open();
            }
            Time.sleep(Random.nextInt(600, 900));
            Bank.depositAllExcept(Main.FoodItem);
            Time.sleep(Random.nextInt(300, 900));
            if (!Bank.contains(Main.FoodItem)) {
                return -1;
            } else {
                Bank.withdrawAll(Main.FoodItem);
                Time.sleep(Random.nextInt(300, 900));
                Bank.close();
            }
        }
        return 300;
    }

    private boolean InBank() {
        if (Main.Edgeville_bank.contains(Players.getLocal()) && !Inventory.contains(Main.FoodItem) && Main.FoodSelected()) {
            return true;
        } else {
            return false;
        }
    }
}
