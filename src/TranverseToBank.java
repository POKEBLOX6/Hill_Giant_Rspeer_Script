import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;

public class TranverseToBank extends Task {

    @Override
    public boolean validate() {
        return NeedToBank();
    }

    @Override
    public int execute() {
        WalkToBank();
        Main.TestText = "Walking to Bank";
        return 300;
    }

    private boolean NeedToBank() {
        if (!Inventory.contains(Main.FoodItem) && Main.FoodSelected()) {
            return true;
        } else {
            return false;
        }
    }


    private void WalkToBank() {
        if (!Main.Edgeville_bank.contains(Players.getLocal())) {
            if (Players.getLocal().getPosition().getY() > 9000) {//In the giant realm
                if (Main.Edgeville_ladder_position_underground.contains(Players.getLocal())) {
                    SceneObjects.getNearest("Ladder").click();
                } else {
                    Movement.walkTo(Main.Edgeville_ladder_position_underground.getCenter());
                }
            } else {
                Movement.walkTo(Main.Edgeville_bank.getCenter());
            }

            Time.sleep(Random.nextInt(1000, 1700));
        }
    }


}
