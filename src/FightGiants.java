import com.rabbitmq.client.Consumer;
import org.rspeer.runetek.adapter.scene.Pickable;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.component.tab.Tab;
import org.rspeer.runetek.api.component.tab.Tabs;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.api.scene.Pickables;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;

public class FightGiants extends Task {

    String[] ItemsToLoot = {"Nature rune", "Cosmic rune", "Death rune", "Chaos rune", "Steel arrow", "Law rune", "Limpwurt root", "Giant key"};

    @Override
    public boolean validate() {

        if (Main.Hill_Giant_Area.contains(Players.getLocal()) && Main.FoodSelected()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int execute() {
        FightGiants();
        return Random.nextInt(900, 1500);

    }


    private void FightGiants() {
        Main.TestText = "Current health Percentage: " + Players.getLocal().getHealthPercent();
        if (Players.getLocal().getHealthPercent() <= 65) {
            Tabs.open(Tab.INVENTORY);
            Time.sleep(Random.nextInt(300, 900));
            Inventory.getFirst(Main.FoodItem).click();
            Time.sleep(Random.nextInt(300, 800));
        }


        for (Pickable pick : Pickables.getLoaded()) {
            for (String s : ItemsToLoot) {
                if (pick.getName().equalsIgnoreCase(s)) {
                    if (!s.equalsIgnoreCase("Limpwurt root") && Inventory.contains(pick.getName()) && !s.equalsIgnoreCase("Giant Key")) {//If already in inventory and stackable, grab.
                        pick.click();
                        System.out.println("Grabbing " + s);
                        Time.sleep(Random.nextInt(3000, 4600));
                    } else if (!Inventory.isFull()) {
                        pick.click();
                        System.out.println("Grabbing " + s);
                        Time.sleep(Random.nextInt(3000, 4600));
                    } else if (pick.getName().equalsIgnoreCase("Giant Key")) {//Found a key? Drop a food item.
                        Inventory.getLast(Main.FoodItem).interact("Drop");
                        Time.sleep(Random.nextInt(300, 1000));
                        pick.click();
                        Time.sleep(Random.nextInt(3000, 4600));
                    }
                }
            }

        }
        if (Players.getLocal().getTarget() == null && !Players.getLocal().isAnimating()) {
            Npcs.getNearest(npc -> npc.getName().equals("Hill Giant") && npc.getTargetIndex() == -1 || npc.getTargetIndex() == Players.getLocal().getIndex()).interact(a -> true);

        }
        Time.sleep(Random.nextInt(300, 900));


    }
}
